'use client'
import { TodoItem } from '@/type/todolist'
import { ChangeEvent, useEffect, useMemo, useState } from 'react'
import { useImmer, useImmerReducer } from 'use-immer'
import './index.scss'

const STORAGE_KEY = 'vue-todomvc'

const filters = {
  all: (todos: TodoItem[]) => todos,
  active: (todos: TodoItem[]) => todos.filter((todo) => !todo.completed),
  completed: (todos: TodoItem[]) => todos.filter((todo) => todo.completed)
}

type AddTodo = { type: 'AddTodo' } & { title: string }
type EditTodo = { type: 'EditTodo' } & { id: number; title: string }
type DeleteTodo = { type: 'DeleteTodo' } & { id: number }
type Toggle = { type: 'Toggle' } & { id: number }
type ToggleAll = { type: 'ToggleAll' }
type RemoveCompleted = { type: 'RemoveCompleted' }
type Restore = { type: 'Restore'; list: TodoItem[] }
type Action = AddTodo | EditTodo | DeleteTodo | Toggle | ToggleAll | RemoveCompleted | Restore
function tasksReducer(draft: TodoItem[], action: Action) {
  switch (action.type) {
    case 'AddTodo': {
      draft.push({
        id: Date.now(),
        title: action.title,
        completed: false
      })
      break
    }
    case 'EditTodo': {
      const item = draft.find((v) => v.id === action.id)!
      item.title = action.title
      break
    }
    case 'DeleteTodo': {
      draft.splice(
        draft.findIndex((v) => v.id === action.id),
        1
      )
      break
    }
    case 'Toggle': {
      const item = draft.find((v) => v.id === action.id)!
      item.completed = !item.completed
      break
    }

    case 'ToggleAll': {
      const allCheck = draft.every((v) => v.completed)
      draft.forEach((todo) => (todo.completed = !allCheck))
      break
    }
    case 'RemoveCompleted': {
      const tmp = filters.active(draft)
      // 清空数组
      draft.length = 0
      draft.push(...tmp)
      break
    }
    case 'Restore': {
      draft.length = 0
      draft.push(...action.list)
      break
    }
  }
}

export default function Home() {
  const [todos, dispatch] = useImmerReducer<TodoItem[], Action>(tasksReducer, [])
  const [visibility, setVisibility] = useState<keyof typeof filters>('all')
  const [editedTodo, setEditedTodo] = useImmer<TodoItem | null>(null)

  const remaining = useMemo(() => filters.active(todos).length, [todos])
  const filteredTodos = useMemo(() => filters[visibility](todos), [todos, visibility])

  function addTodo(target: HTMLInputElement): void {
    const input = target
    const value = input.value.trim()
    if (value) {
      dispatch({
        type: 'AddTodo',
        title: value
      })
      input.value = ''
    }
  }

  function editTodo(todo: TodoItem) {
    setEditedTodo(JSON.parse(JSON.stringify(todo)))
  }

  function removeTodo(todo: TodoItem) {
    dispatch({
      type: 'DeleteTodo',
      id: todo.id
    })
  }

  function doneEdit(todo: TodoItem) {
    const title = editedTodo!.title.trim()
    if (title) {
      dispatch({
        type: 'EditTodo',
        id: todo.id,
        title: title
      })
    } else removeTodo(todo)
    setEditedTodo(null)
  }

  function cancelEdit() {
    setEditedTodo(null)
  }

  function removeCompleted() {
    dispatch({
      type: 'RemoveCompleted'
    })
  }

  useEffect(() => {
    const list: TodoItem[] = JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]')
    dispatch({
      type: 'Restore',
      list: list
    })
  }, [dispatch])

  // 状态持久化
  useEffect(() => {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(todos))
  }, [todos])

  useEffect(() => {
    function onHashChange() {
      const route = window.location.hash.replace(/#\/?/, '') as keyof typeof filters
      if (filters[route]) {
        setVisibility(route)
      } else {
        window.location.hash = ''
        setVisibility('all')
      }
    }

    // 处理路由
    window.addEventListener('hashchange', onHashChange)
    onHashChange()

    return () => {
      window.removeEventListener('hashchange', onHashChange)
    }
  }, [])

  return (
    <section className="todoapp">
      <header className="header">
        <h1>Todos Reducer</h1>
        <input
          className="new-todo"
          autoFocus
          placeholder="What needs to be done?"
          onKeyUp={(e) => e.key === 'Enter' && addTodo(e.target as HTMLInputElement)}
        />
      </header>
      <section className="main" hidden={!todos.length}>
        <input
          id="toggle-all"
          className="toggle-all"
          type="checkbox"
          checked={remaining === 0}
          onChange={() =>
            dispatch({
              type: 'ToggleAll'
            })
          }
        />
        <label htmlFor="toggle-all">Mark all as complete</label>
        <ul className="todo-list">
          {filteredTodos.map((todo) => (
            <li
              key={todo.id}
              className={`todo ${todo.completed ? 'completed' : ''} ${
                todo.id === editedTodo?.id ? 'editing' : ''
              }`}
            >
              <div className="view">
                <input
                  className="toggle"
                  type="checkbox"
                  checked={todo.completed}
                  onChange={() => {
                    dispatch({
                      type: 'Toggle',
                      id: todo.id
                    })
                  }}
                />
                <label onDoubleClick={() => editTodo(todo)}>{todo.title}</label>
                <button className="destroy" onClick={() => removeTodo(todo)}></button>
              </div>
              {todo.id === editedTodo?.id && (
                <input
                  className="edit"
                  type="text"
                  value={editedTodo.title}
                  onChange={(e) => {
                    setEditedTodo((draft) => {
                      draft!.title = e.target.value
                    })
                  }}
                  autoFocus
                  onBlur={() => doneEdit(todo)}
                  onKeyUp={(e) => {
                    if (e.key === 'Enter') doneEdit(todo)
                    else if (e.key === 'Escape') cancelEdit()
                  }}
                />
              )}
            </li>
          ))}
        </ul>
      </section>
      <footer className="footer" hidden={!todos.length}>
        <span className="todo-count">
          <strong>{remaining}</strong>
          <span> item{remaining === 1 ? '' : 's'} left</span>
        </span>
        <ul className="filters">
          <li>
            <a href="#/all" className={visibility === 'all' ? 'selected' : ''}>
              All
            </a>
          </li>
          <li>
            <a href="#/active" className={visibility === 'active' ? 'selected' : ''}>
              Active
            </a>
          </li>
          <li>
            <a href="#/completed" className={visibility === 'completed' ? 'selected' : ''}>
              Completed
            </a>
          </li>
        </ul>
        <button
          className="clear-completed"
          onClick={removeCompleted}
          hidden={todos.length <= remaining}
        >
          Clear completed
        </button>
      </footer>
    </section>
  )
}
