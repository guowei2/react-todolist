import Link from 'next/link'

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      maste
      <Link href="/todolist">todolist</Link>
      <Link href="/todolist-hook">todolist-hook</Link>
      <Link href="/todolist-reducer">todolist-reducer</Link>
    </main>
  )
}
