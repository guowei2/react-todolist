'use client'
import '../todolist/index.scss'
import TodoListHooks from './todolist'

export default function Home() {
  const {
    todos,
    remaining,
    filteredTodos,
    editedTodo,
    visibility,
    addTodo,
    toggleAll,
    editTodo,
    removeTodo,
    doneEdit,
    cancelEdit,
    removeCompleted,
    setTodos,
    setEditedTodo
  } = TodoListHooks()

  return (
    <section className="todoapp">
      <header className="header">
        <h1>Todos hook</h1>
        <input
          className="new-todo"
          autoFocus
          placeholder="What needs to be done?"
          onKeyUp={(e) => e.key === 'Enter' && addTodo(e.target as HTMLInputElement)}
        />
      </header>
      <section className="main" hidden={!todos.length}>
        <input
          id="toggle-all"
          className="toggle-all"
          type="checkbox"
          checked={remaining === 0}
          onChange={toggleAll}
        />
        <label htmlFor="toggle-all">Mark all as complete</label>
        <ul className="todo-list">
          {filteredTodos.map((todo) => (
            <li
              key={todo.id}
              className={`todo ${todo.completed ? 'completed' : ''} ${
                todo.id === editedTodo?.id ? 'editing' : ''
              }`}
            >
              <div className="view">
                <input
                  className="toggle"
                  type="checkbox"
                  checked={todo.completed}
                  onChange={(e) => {
                    setTodos((draft) => {
                      const v = draft.find((t) => t.id === todo.id)!
                      v.completed = e.target.checked
                    })
                  }}
                />
                <label onDoubleClick={() => editTodo(todo)}>{todo.title}</label>
                <button className="destroy" onClick={() => removeTodo(todo)}></button>
              </div>
              {todo.id === editedTodo?.id && (
                <input
                  className="edit"
                  type="text"
                  value={editedTodo.title}
                  onChange={(e) => {
                    setEditedTodo((draft) => {
                      draft!.title = e.target.value
                    })
                  }}
                  autoFocus
                  onBlur={() => doneEdit(todo)}
                  onKeyUp={(e) => {
                    if (e.key === 'Enter') doneEdit(todo)
                    else if (e.key === 'Escape') cancelEdit()
                  }}
                />
              )}
            </li>
          ))}
        </ul>
      </section>
      <footer className="footer" hidden={!todos.length}>
        <span className="todo-count">
          <strong>{remaining}</strong>
          <span> item{remaining === 1 ? '' : 's'} left</span>
        </span>
        <ul className="filters">
          <li>
            <a href="#/all" className={visibility === 'all' ? 'selected' : ''}>
              All
            </a>
          </li>
          <li>
            <a href="#/active" className={visibility === 'active' ? 'selected' : ''}>
              Active
            </a>
          </li>
          <li>
            <a href="#/completed" className={visibility === 'completed' ? 'selected' : ''}>
              Completed
            </a>
          </li>
        </ul>
        <button
          className="clear-completed"
          onClick={removeCompleted}
          hidden={todos.length <= remaining}
        >
          Clear completed
        </button>
      </footer>
    </section>
  )
}
