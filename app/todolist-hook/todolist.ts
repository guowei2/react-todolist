import { TodoItem } from '@/type/todolist'
import { ChangeEvent, useEffect, useMemo, useState } from 'react'
import { useImmer } from 'use-immer'

const STORAGE_KEY = 'vue-todomvc'

const filters = {
  all: (todos: TodoItem[]) => todos,
  active: (todos: TodoItem[]) => todos.filter((todo) => !todo.completed),
  completed: (todos: TodoItem[]) => todos.filter((todo) => todo.completed)
}

export default function TodoListHooks() {
  const [todos, setTodos] = useImmer<TodoItem[]>([])
  const [visibility, setVisibility] = useState<keyof typeof filters>('all')
  const [editedTodo, setEditedTodo] = useImmer<TodoItem | null>(null)

  const remaining = useMemo(() => filters.active(todos).length, [todos])
  const filteredTodos = useMemo(() => filters[visibility](todos), [todos, visibility])

  function addTodo(target: HTMLInputElement): void {
    const input = target
    const value = input.value.trim()
    if (value) {
      setTodos((draft) => {
        draft.push({
          id: Date.now(),
          title: value,
          completed: false
        })
      })
      input.value = ''
    }
  }

  function toggleAll(e: ChangeEvent<HTMLInputElement>) {
    const checked = e.target.checked
    setTodos((draft) => {
      draft.forEach((todo) => (todo.completed = checked))
    })
  }

  function editTodo(todo: TodoItem) {
    setEditedTodo(JSON.parse(JSON.stringify(todo)))
  }

  function removeTodo(todo: TodoItem) {
    setTodos((draft) => {
      draft.splice(draft.indexOf(todo), 1)
    })
  }

  function doneEdit(todo: TodoItem) {
    const title = editedTodo!.title.trim()
    if (title) {
      setTodos((draft) => {
        const item = draft.find((v) => v.id === todo.id)!
        item.title = title
      })
    } else removeTodo(todo)
    setEditedTodo(null)
  }

  function cancelEdit() {
    setEditedTodo(null)
  }

  function removeCompleted() {
    setTodos(filters.active(todos))
  }

  useEffect(() => {
    setTodos(JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]'))
  }, [setTodos])

  // 状态持久化
  useEffect(() => {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(todos))
  }, [todos])

  useEffect(() => {
    function onHashChange() {
      const route = window.location.hash.replace(/#\/?/, '') as keyof typeof filters
      if (filters[route]) {
        setVisibility(route)
      } else {
        window.location.hash = ''
        setVisibility('all')
      }
    }

    // 处理路由
    window.addEventListener('hashchange', onHashChange)
    onHashChange()
    return () => {
      window.removeEventListener('hashchange', onHashChange)
    }
  }, [])

  return {
    todos,
    remaining,
    filteredTodos,
    editedTodo,
    visibility,
    addTodo,
    toggleAll,
    editTodo,
    removeTodo,
    doneEdit,
    cancelEdit,
    removeCompleted,
    setTodos,
    setEditedTodo
  }
}
