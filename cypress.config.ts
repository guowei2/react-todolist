import { defineConfig } from "cypress";

export default defineConfig({
  projectId: "exmu8y",
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    specPattern: 'cypress/e2e/**/*.{cy,spec}.ts',
    baseUrl: 'http://localhost:3000/react-todolist'
  },
});
