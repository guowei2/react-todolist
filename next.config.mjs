/** @type {import('next').NextConfig} */
const nextConfig = {
    basePath: '/react-todolist',
    output: 'export'
};

export default nextConfig;
